const settings = {
  name: "audrey-portfolio",
  state: {
    frontity: {
      url: "https://audreyhusson.com",
      title: "Ah - Ha",
      description: "Bonjour, Je suis Audrey, Directrice Artistique freelance Print et Web basée à Lyon."
    }
  },
  packages: [
    "@frontity/tiny-router",
    "@frontity/html2react",
    {
      name: "ah-ha-theme"
    },
    {
      name: "@frontity/wp-source",
      state: {
        source: {
          url: "https://admin.audreyhusson.com",
          homepage: "/accueil",
          postPage: "/blog",
          postTypes: [
            {
              type: "projet",
              endpoint: "projet",
              archive: "/projets"
            }
          ],
        }
      }
    },
  ]
};

export default settings;
