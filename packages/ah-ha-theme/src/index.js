import Root from "./components/app"
import helloBlock from "./components/Organisms/HelloBlock"
import wpPostDescription from "./components/Organisms/PostDescription"
import wpPostFooter from "./components/Organisms/postFooter"
import wpPostGallery from "./components/Organisms/PostGallery"
import wpPostHeader from "./components/Organisms/PostHeader"
import wpPostHeroBanner from "./components/Organisms/PostHeroBanner"

const AudreyPortfolio = {
  name: "ah-ha-theme",
  roots: {
    theme: Root,
  },
  state: {
    theme: {},
  },
  actions: {
    theme: {},
  },
  libraries: {
    html2react: {
      processors: [
        helloBlock,
        wpPostDescription,
        wpPostFooter,
        wpPostGallery,
        wpPostHeader,
        wpPostHeroBanner,
      ]
    }
  }
}

export default AudreyPortfolio