import React from "react"
import { connect, styled } from "frontity"
import { motion } from "framer-motion"
import Logo from "../Molecules/Logo"

const Overlay = () => {

    return (
        <OverlayContainer
            animate="in"
            exit="out"
            initial="out"
            variants={OverlayTransition}
        >
        </OverlayContainer>
    )
}

const OverlayContainer = styled(motion.div)`
    background-color: var(--dark);
    bottom: 0;
    color: red;
    height: 100%;
    left: 0;
    position: fixed;
    width: 0;
`;

const OverlayTransition = {
    in: () => ({
        width: 0,

        transition: {
            delay: 1,
            duration: 1,
            ease: [.86, 0, .07, 1]
        },
    }),
    out: () => ({
        width: "100%",

        transition: {
            delay: 1,
            duration: 1,
            ease: [.77, 0, .18, 1]
        },
    })
};

export default connect(Overlay)