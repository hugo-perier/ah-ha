import React from "react"
import { styled } from "frontity"
import { motion } from "framer-motion"

const HelloBlock = ({ content }) => {

    return (
        <HelloBlockContainer dangerouslySetInnerHTML={{ __html: content }} />
    )
}

const HelloBlockContainer = styled(motion.h1)`
    font-family: 'Self Modern', serif;
    font-size: 6.25rem;
    font-weight: 400;
    margin: 0;
    max-width: 78.75rem;
    text-align: center;

    strong{
        font-family: 'DM Sans', sans-serif;
        font-weight: 700;
    }

    @media screen and (max-width: 1000px){
        font-size: 4rem;
    }
    
    @media screen and (max-width: 800px){
        font-size: 2rem;
    }
`;

const helloBlock = {
    name: 'helloBlock',
    priority: 20,
    test: ({ node }) => node.component === "section" && node.props.className === "HelloBlock",
    processor: ({ node }) => {
        const content = JSON.parse(JSON.parse(node.props["data-attribute"]));

        return {
            component: HelloBlock,
            props: { content },
        }
    },
}

export default helloBlock;
