import React from "react"
import { connect, styled } from "frontity"
import Title2 from "../Molecules/Title2";
import Cta from "../Molecules/Cta";
import PostFigure from "../Molecules/PostFigure";

const ImageText = () => {
    return (
        <ImageTextContainer>
            <figure>
                <img src={"https://picsum.photos/440/640"} alt="test" />
            </figure>
            <div>
                <Title2 label="Qui suis-je ?" />
                <p>After 3.5 years living in London I came back home, in Lyon, in November 2019. I’ve learnt so much during my time in the UK both on a professional and personal level. I was employed by a digital agency for 2 years and then started a new adventure into the freelance world.</p>
                <p>I’m currently working in Lyon both for English and French clients as freelancer.</p>
                <p>I now have 5 years experience and will be happy to help and support you in your branding and digital projects.</p>
                <p>The design world fascinates me everyday keeping my creativity and curiosity alive, please feel free to contact me for any opportunities. I’d be happy to chat.</p>
                <p>À bientôt</p>
                <Cta label="Me contacter" anchor="/contact" />
            </div>
        </ImageTextContainer>
    )
}

const ImageTextContainer = styled.section`
    margin: 6.25rem var(--body-margin);

    figure{
        margin: 0 0 3rem;
    }

    p{
        min-width: 18.75rem;
        width: 66%;
    }

    @media screen and (min-width: 800px){
        display: flex;

        figure, > div{
            width: 50%;
        }

        figure{
            box-sizing: border-box;
            padding: 0 8%;
            margin: 0;
        }
    }
`;

export default connect(ImageText)