import React, { useState } from "react"
import { connect, styled } from "frontity"
import { motion } from "framer-motion"

import NavButton from "../Molecules/NavButton"
import MenuItem from "../Molecules/MenuItem"
import Logo from "../Molecules/Logo"

const Menu = () => {

    var [menuState, setMenuState] = useState(false)
    const toggleMenu = () => setMenuState(!menuState)

    return (
        <MenuContainer onClick={toggleMenu}>
            <NavButton menuState={menuState} />
            <ul className={menuState ? "open" : "close"}>
                <li>
                    <Logo />
                </li>
                <li>
                    <MenuItem anchor="/projets" label="Work" />
                </li>
                <li>
                    <MenuItem anchor="/about-us" label="About" />
                </li>
                <li>
                    <MenuItem anchor="/" label="Contact" />
                </li>
            </ul>
        </MenuContainer>
    )
}

const MenuContainer = styled(motion.nav)`
    ul{
        align-items: center;
        background: linear-gradient(to bottom, var(--dark) 50%, var(--light) 50%) 0 0 / 100% 200%;
        bottom: 100%;
        box-sizing: border-box;
        display: flex;
        flex-direction: column;
        justify-content: center;
        font-size: 3.125rem;
        height: 100%;
        list-style-type: none;
        margin: 0;
        padding: 3.75rem;
        position: fixed;
        left: 0;
        transition: 
            bottom 850ms cubic-bezier(.69,.01,.31,1),
            background-position 500ms cubic-bezier(.69,.01,.31,1) 200ms;
        width: 100%;
        z-index: 99;

        li:nth-of-type(1), li:nth-of-type(4){
            margin-bottom: auto;
        }

        li + li{
            overflow: hidden;

            &:nth-of-type(2) a{
                transition-delay: 1600ms;
            }

            &:nth-of-type(3) a{
                transition-delay: 1800ms;
            }

            &:nth-of-type(4) a{
                transition-delay: 2000ms;
            }

            a{
                display: block;
                opacity: 0;
                transform: translateY(150%) rotate(10deg);
                transform-origin: center;
                transition: 
                    transform 600ms ease,
                    opacity 600ms ease;
            }
        }

        &.open{
            background-position: 0 -100%;
            bottom: 0;
            transition: 
                bottom 1000ms cubic-bezier(.69,.01,.31,1),
                background-position 500ms cubic-bezier(.69,.01,.31,1) 850ms;

            li + li{
                a{
                    opacity: 1;
                    transform: translateY(0) rotate(0);
                }
            }
        }
    }
`;

export default connect(Menu)