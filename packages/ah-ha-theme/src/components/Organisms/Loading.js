import React from "react"
import { connect, styled } from "frontity"
import { motion } from "framer-motion"

import Logo from "../Molecules/Logo"

const Loading = () => {

    return (
        <LoadingContainer>
            <motion.div
                initial={{ opacity: 0, scale: 0.5 }}
                animate={{ opacity: 1, scale: 1 }}
                transition={{ duration: 1 }}
            >
                <Logo />
            </motion.div>
        </LoadingContainer>
    )
}

const LoadingContainer = styled(motion.div)`
    align-items: center;
    background-color: var(--dark);
    bottom: 0;
    color: red;
    display: flex;
    height: 100%;
    justify-content: center;
    left: 0;
    position: fixed;
    width: 100%;
`;

export default connect(Loading)