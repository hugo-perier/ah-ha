import React from "react"
import { connect, styled } from "frontity"
import { motion } from "framer-motion"

import Cta from "../Molecules/Cta"
import ArticleTitle from "../Molecules/ArticleTitle"
import Tags from "../Molecules/Tags"
import FeaturedImage from "../Molecules/FeaturedImage"

const Article = ({ item, state }) => {

    return (
        <ArticleContainer>
            {console.log(item)}
            <FeaturedImage id={item.featured_media} />
            <div>
                <ArticleTitle label={item.title.rendered} />
                <div>
                    {item.categories.map(category => {
                        const cat = state.source.category[category]
                        return (
                            <Tags key={cat.id} label={cat.name} />
                        )
                    })}
                    <Cta anchor={item.link} label="Voir projet" />
                </div>
            </div>
        </ArticleContainer>
    )
}

const ArticleContainer = styled(motion.article)`
    margin: 0 var(--body-margin);
    position: relative;

    > div{
        display: flex;
        flex-direction: column;
        justify-content: center;
        margin-top: 2rem;
        row-gap: 1.875rem;

        > div{
            align-items: center;
            column-gap: 1.5rem;
            display: flex;
            flex-wrap: wrap;
            row-gap: 1rem;

            &::after{
                background-color: currentColor;
                content: "";
                display: block;
                height: 1px;
                flex-grow: 1;
                margin-top: .1em;
            }

            .cta{
                order: 2;
                white-space: nowrap;
        
                &::before{
                    content: "";
                    height: 100%;
                    left: 0;
                    position: absolute;
                    top: 0;
                    width: 100%;
                }
            }
        }
    }

    img{
        transition: transform 400ms ease;
    }

    &:hover, &:focus{
        img{
            transform: scale(1.1);
        }
    }


    @media screen and (max-width: 750px){
        .cta{
            opacity: 0;
            padding: 0;
        }
    }


    @media screen and (min-width: 950px){
        display: flex;
        justify-content: space-between;

        figure{
            width: 55%;
        }

        > div{
            margin-top: 0;
            width: 40%;
        }

    }
`;

export default connect(Article)