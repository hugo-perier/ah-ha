import React from "react"
import { styled } from "frontity"

import PostFigure from "../Molecules/PostFigure"
import TextColumn from "../Molecules/TextColumn"

const PostDescription = ({ data, img }) => {
    return (
        <PostDescriptionContainer>
            <div>
                <h3>{data.titre}</h3>
                <TextColumn data={data.paragraphs} />
            </div>
            <PostFigure url={img.url} alt={img.alt} />
        </PostDescriptionContainer>
    )
}

const PostDescriptionContainer = styled.section`
    margin: 6.25rem var(--body-margin) 3.25rem;

    @media screen and (min-width: 950px){
        align-items: center;
        display: flex;

        > *{
            width: 50%;
        }

        figure{
            box-sizing: border-box;
            padding-left: 10%;
        }
    }
`;

const postDescription = {
    name: 'postDescription',
    priority: 20,
    test: ({ node }) => node.component === "section" && node.props.className === "PostDescription",
    processor: ({ node }) => {

        const data = {
            titre: node.children[0].children[0].content,
            paragraphs: Object.values(node.children[1].children),
        };

        const img = {
            url: node.props["data-image-url"],
            alt: node.props["data-image-alt"],
        };

        return {
            component: PostDescription,
            props: { data, img },
        }

    },
}

export default postDescription;