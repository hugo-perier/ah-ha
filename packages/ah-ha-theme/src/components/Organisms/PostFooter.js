import React from "react"
import { styled } from "frontity"
import Link from "@frontity/components/link"

const PostFooter = ({ data }) => {
    return (
        <PostFooterContainer>
            <p>
                {data.paragraph}
                <Link link={data.link}>
                    {data.label}
                </Link>
            </p>
        </PostFooterContainer>
    )
}

const PostFooterContainer = styled.section`
    background-color: var(--dark);
    margin: 7.5rem 0 0;
    padding: 10.25rem var(--body-margin);
    position: relative;

    p{
        align-items: center;
        border: solid 1px transparent;
        border-radius: 5rem;
        color: var(--light);
        column-gap: var(--body-margin);
        display: flex;
        justify-content: center;
        padding: 2.5rem 5rem;
        transition: border-color 500ms ease 200ms;
        
        &:hover, &:focus{
            border-color: currentColor;
            
            &::after{
                flex-basis: 18rem;
            }
        }
        
        a{
            color: inherit;  
            font-family: 'Self Modern', serif;
            font-size: 2.5rem;
            font-style: italic;
            font-weight: 400;
            order: 2;
            text-decoration: none;

            &::before{
                content: "";
                height: 100%;
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
            }
        }


        &::after{
            background-color: currentColor;
            content: "";
            display: block;
            height: 1px;
            flex-basis: 15rem;
            margin-top: .1em;
            transition: flex-basis 500ms ease 200ms;
        }
    }

    @media screen and (max-width: 750px){
        p{
            flex-direction: column;
         
            &::after{
                height: 1px;
                flex-basis: unset;
                width: 80%;
            }
        }
    }

    @media screen and (min-width: 950px){
        p{
            margin: 0 var(--body-margin);
        }
    }
`;

const wpPostFooter = {
    name: 'wpPostFooter',
    priority: 20,
    test: ({ node }) => node.component === "section" && node.props.className === "PostFooter",
    processor: ({ node }) => {

        const data = {
            paragraph: node.children[0].children[0].content,
            label: node.children[1].children[0].content,
            link: node.children[1].props.href,
        };

        return {
            component: PostFooter,
            props: { data },
        }

    },
}

export default wpPostFooter;