import React from "react"
import { styled } from "frontity"
import Image from "@frontity/components/image";

const PostHeroBanner = ({ img }) => {
    return (
        <PostHeroBannerContainer>
            <Image
                alt={img.alt}
                src={img.url}
            />
        </PostHeroBannerContainer>
    )
}

const PostHeroBannerContainer = styled.figure`
    height: 640px;
    margin: 6.25rem 0;
    max-height: 120vw;
    overflow: hidden;
    width: 100%;

    img{
        height: 100%;
        object-fit: cover;
        width: 100%;
    }
`;

const wpPostHeroBanner = {
    name: 'wpPostHeroBanner',
    priority: 20,
    test: ({ node }) => node.component === "section" && node.props.className === "PostHeroBanner",
    processor: ({ node }) => {
        const img = {
            url: node.props["data-image-url"],
            alt: node.props["data-image-alt"],
        };

        return {
            component: PostHeroBanner,
            props: { img },
        }
    },
}

export default wpPostHeroBanner;