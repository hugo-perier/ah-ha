import React from "react"
import { connect, styled } from "frontity"
import Title2 from "../Molecules/Title2";

const TwoColumn = () => {
    return (
        <TwoColumnContainer>
            <div>
                <Title2 label="Le projet" />
                <ul>
                    <li>Direction artistique</li>
                    <li>Identité visuelle</li>
                    <li>Branding</li>
                    <li>UI design</li>
                    <li>Réseaux sociaux</li>
                </ul>
            </div>
            <div>
                <Title2 label="Ils m'ont fait confiance" />
                <ul>
                    <li>Direction artistique</li>
                    <li>Identité visuelle</li>
                    <li>Branding</li>
                    <li>UI design</li>
                    <li>Réseaux sociaux</li>
                </ul>
                <p>Et bien plus encore…</p>
            </div>
        </TwoColumnContainer>
    )
}

const TwoColumnContainer = styled.section`
    margin: 6.25rem var(--body-margin);

    > *{
        margin-bottom: 6.25rem;
    }

    @media screen and (min-width: 950px){
        align-items: flex-start;
        display: flex;

        > *{
            width: 50%;

            &:first-of-type{
                padding-left: 8%;
            }

            &:last-of-type{
                padding-right: 8%;
            }
        }

        ul{
            font-family: 'Self Modern', serif;
            font-style: italic;
            list-style-type: none;
            padding-left: 0;
        }
    }
`;

export default connect(TwoColumn)