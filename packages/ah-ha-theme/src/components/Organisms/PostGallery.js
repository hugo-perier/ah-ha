import React from "react"
import { styled } from "frontity"

import PostFigure from "../Molecules/PostFigure";

const PostGallery = ({ data }) => {
    return (
        <PostGalleryContainer>
            {data.map((item, key) =>
                <PostFigure alt={item.props["alt"]} key={key} url={item.props["src"]} width={item.props["className"]} />
            )}
        </PostGalleryContainer>
    )
}

const PostGalleryContainer = styled.section`
    align-items: center;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    margin: 3.25rem var(--body-margin) 6.25rem;
    row-gap: 3.25rem;
`;


const wpPostGallery = {
    name: 'wpPostGallery',
    priority: 20,
    test: ({ node }) => node.component === "section" && node.props.className === "PostGallery",
    processor: ({ node }) => {

        const data = Object.values(node.children);

        return {
            component: PostGallery,
            props: { data },
        }

    },
}

export default wpPostGallery;