import React from "react"
import { connect, styled } from "frontity"
import Tags from "../Molecules/Tags"
import Title1 from "../Molecules/Title1"
import Cta from "../Molecules/Cta.js"

const Contact = () => {
    return (
        <ContactContainer>
            <div>
                <Title1 label="Écrivez-moi" />
            </div>
            <div>
                <dl><Tags string label="Audrey Husson" /></dl>
                <dt>Direction artistique Print et Web</dt>
                <address>
                    <a href="tel:+33644137175"><Tags label="06.44.13.71.75" /></a>
                    <a href="mailto:bonjour@audreyhusson.com"><Tags label="bonjour@audreyhusson.com" /></a>
                </address>
                <Cta label="Voir mes projets" anchor="/work" />
            </div>
        </ContactContainer>
    )
}

const ContactContainer = styled.section`
    margin: 6.25rem var(--body-margin);

    > div{
        margin-bottom: 6.25rem;

        &:last-of-type{
            margin-top: 7.5rem;
        }
    }

    dl{
        margin: 0;
    }

    dt{
        font-family: 'Self Modern', serif;
        font-style: italic;
    }

    address{
        margin: 2em 0;

        a{
            display: block;
            color: inherit;
            text-decoration: none;
        }
        font-style: normal;
    }

    @media screen and (min-width: 950px){
        align-items: flex-start;
        display: flex;

        > *{
            width: 50%;

            &:first-of-type{
                padding-left: 8%;
            }

            &:last-of-type{
                padding: 0 8%;
            }
        }

        ul{
            font-family: 'Self Modern', serif;
            font-style: italic;
            list-style-type: none;
            padding-left: 0;
        }
    }
`;

export default connect(Contact)