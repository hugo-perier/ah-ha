import React from "react"
import { styled } from "frontity"

import ArticleTitle from "../Molecules/ArticleTitle"
import Tags from "../Molecules/Tags"
import Image from "@frontity/components/image";

const PostHeader = ({ img }) => {
    return (
        <PostHeaderContainer>
            <Image
                alt={img.alt}
                src={img.url}
            />
            <div>
                <ArticleTitle label="Génération Terrasse" />
                <Tags label="Branding" />
            </div>
        </PostHeaderContainer>
    )
}

const PostHeaderContainer = styled.section`
    margin: 0 var(--body-margin) 6.25rem;

    > div{
        display: flex;
        flex-direction: column;
        justify-content: center;
        margin-top: 2rem;
        row-gap: 1.875rem;
    }

    @media screen and (min-width: 950px){
        display: flex;
        justify-content: space-between;

        figure{
            width: 55%;
        }

        > div{
            margin-top: 0;
            width: 40%;
        }

    }
`;

const wpPostHeader = {
    name: 'wpPostHeader',
    priority: 20,
    test: ({ node }) => node.component === "section" && node.props.className === "PostHeader",
    processor: ({ node }) => {
        const img = {
            url: node.props["data-image-url"],
            alt: node.props["data-image-alt"],
        };

        return {
            component: PostHeader,
            props: { img },
        }
    },
}

export default wpPostHeader;
