import React from "react"
import { connect, styled } from "frontity"
import Link from "@frontity/components/link"

const Cta = ({ anchor, label }) => {
    return (
        <CtaContainer className="cta" link={anchor}>
            {label}
        </CtaContainer>
    )
}

const CtaContainer = styled(Link)`
    background: linear-gradient(to top, var(--dark) 50%, transparent 50%) 0 0 / 100% 200% no-repeat;
    border: solid 1px var(--dark);
    border-radius: 3.5rem;
    color: var(--dark);
    display: inline-block;
    font-size: 1.125rem;
    padding: 1.25rem 3.5rem;
    text-decoration: none;
    transition: background-position 400ms ease, color 400ms ease 200ms, padding 200ms ease;

    &:hover, &:focus{
        background-position: 0 100%;
        color: var(--light);
        padding: 1.35rem 3.5rem 1.15rem;
    }
`;

export default connect(Cta)