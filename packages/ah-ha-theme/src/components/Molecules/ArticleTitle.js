import React from "react"
import { connect, styled } from "frontity"

const ArticleTitle = ({ label }) => {
    return (
        <ArticleTitleContainer>
            {label}
        </ArticleTitleContainer>
    )
}

const ArticleTitleContainer = styled.h2`
    font-family: 'Self Modern', serif;
    font-size: 8.75rem;
    font-weight: 400;
    margin: 0;

    @media screen and (max-width: 1800px){
        font-size: 6rem;
    }

    @media screen and (max-width: 1400px){
        font-size: 4rem;
    }

    @media screen and (max-width: 600px){
        font-size: 2rem;
    }
`;

export default connect(ArticleTitle)