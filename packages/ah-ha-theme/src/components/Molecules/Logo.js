import React from "react"
import { connect, styled } from "frontity"
import Link from "@frontity/components/link"

const Logo = () => {

    return (
        <LogoContainer link="/">
            <span>Ah</span>
            <span>Ha</span>
        </LogoContainer>
    )
}

const LogoContainer = styled(Link)`
    align-items: center;
    background: linear-gradient(to bottom, transparent 9%, currentColor 9%, currentColor 41%, transparent 41%) 50% 0 / 1px 200% repeat-y;
    color: inherit;
    column-gap: .45em;
    display: flex;
    font-family: 'Self Modern', serif;
    font-size: 2.5rem;
    font-weight: 400;
    margin: 0 auto;
    text-decoration: none;
    transition: 
        background-position .75s cubic-bezier(.79, .14, .15, .86) .2s,
        column-gap .5s ease .2s;
    width: min-content;

    &:hover, &:focus{
        background-position: 50% -200%;
        column-gap: .75em;
    }
`;

export default connect(Logo)