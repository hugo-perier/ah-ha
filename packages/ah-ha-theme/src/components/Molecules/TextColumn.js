import React from "react"
import { connect, styled } from "frontity"

const TextColumn = ({ data }) => {
    return (
        <TextColumnContainer>
            {data.map((item, key) =>
                <p key={key}>{item.children[0].content}</p>
            )}
        </TextColumnContainer>
    )
}

const TextColumnContainer = styled.div`
    @media screen and (min-width: 600px){
        columns: 2;

        p{
            break-inside: avoid;
            margin-top: 0;
        }
    }
`;

export default connect(TextColumn)