import React from "react"
import { connect, styled } from "frontity"
import Link from "@frontity/components/link"

const MenuItem = ({ anchor, label }) => {
    return (
        <MenuItemContainer link={anchor}>
            {label}
        </MenuItemContainer>
    )
}

const MenuItemContainer = styled(Link)`
    font-family: 'Self Modern', serif;
    color: var(--dark);
    font-size: 9.375rem;
    text-decoration: none;

    &:hover, &:focus{
        font-family: 'DM Sans', sans-serif;
        font-weight: 700;
    }
`;

export default connect(MenuItem)