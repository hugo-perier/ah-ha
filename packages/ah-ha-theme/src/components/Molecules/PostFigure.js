import React from "react"
import { connect, styled } from "frontity"
import Image from "@frontity/components/image";

const PostFigure = ({ alt, url, width }) => {
    return (
        <PostFigureContainer className={width}>
            <Image
                alt={alt}
                src={url}
            />
            {alt ?
                <figcaption>{alt}</figcaption>
                : ''}
        </PostFigureContainer>
    )
}

const PostFigureContainer = styled.figure`
    margin: 0;

    img{
        margin: 0 auto;
    }

    figcaption{
        margin-top: 1rem;
    }

    &.of1, &.of2, &.of3{
        margin: 0 auto;
        width: auto;
    }

    @media screen and (min-width: 650px){
        &.of2, &.of3{
            width: calc((100% - 1rem) / 2);
        }
    }

    @media screen and (min-width: 950px){
        &.of3{
            width: calc((100% - 1rem * 2) / 3);
        }
    }

`;

export default connect(PostFigure)