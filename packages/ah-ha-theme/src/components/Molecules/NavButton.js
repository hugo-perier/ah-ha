import React, { useState } from "react"
import { connect, styled } from "frontity"
import { motion } from "framer-motion"

const NavButton = ({ menuState }) => {

    return (
        <NavButtonContainer>
            <input className="menu-mobile-button" type="checkbox" checked={menuState} readOnly />
            <span className="menu-mobile-button">
                <span />
                <span />
                <span />
            </span>
        </NavButtonContainer>
    )
}

const NavButtonContainer = styled(motion.div)`
    .menu-mobile-button {
        align-items: center;
        cursor: pointer;
        display: flex;
        flex-direction: column;
        height: 3.25rem;
        justify-content: center;
        position: fixed;
        right: 8%;
        row-gap: 12px;
        top: 3.75rem;
        transition: 
            row-gap 750ms ease 1150ms,
            transform 1000ms cubic-bezier(0.68,-0.55,0.27,1.55) 900ms;
        width: 3.25rem;
        z-index: 100;

        span{
            background-color: var(--dark);
            border-radius: 2px;
            display: block;
            height: 4px;
            transition:
                opacity 250ms ease 1400ms,
                transform 500ms ease 1150ms, 
                width 250ms ease;
            transform-origin: center;
            width: 35px;

            &:nth-of-type(2){
                margin: -2px auto;
            }
        }
    }

    input.menu-mobile-button{
        border: 0;
        box-shadow: none;
        margin: 0;
        opacity: 0;
        outline: none; 
        z-index: 101;

        &:checked{
            ~ span.menu-mobile-button{
                row-gap: 0;
                transform: rotate(360deg);

                span:nth-of-type(1){
                    transform: translateY(2px) rotate(45deg);
                }

                span:nth-of-type(3){
                    transform: translateY(-2px) rotate(-45deg);
                }

                span:nth-of-type(2){
                    opacity: 0;
                }
            }
        }

        &:hover{
            ~ span.menu-mobile-button span{
                width: 45px;
            }
        }
    }
`;

export default connect(NavButton)