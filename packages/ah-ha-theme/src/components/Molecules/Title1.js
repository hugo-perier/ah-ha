import React from "react"
import { connect, styled } from "frontity"

const Title1 = ({ label }) => {
    return (
        <Title1Container>
            {label}
        </Title1Container>
    )
}

const Title1Container = styled.h1`
    font-family: 'Self Modern', serif;
    font-size: 9.375rem;
    font-weight: 400;
    margin: 0;

    @media screen and (max-width: 1200px){
        font-size: 4rem;
    }
    
    @media screen and (max-width: 600px){
        font-size: 2rem;
    }
`;

export default connect(Title1)