import React from "react"
import { connect, styled } from "frontity"

const Title3 = ({ label }) => {
    return (
        <Title3Container>
            {label}
        </Title3Container>
    )
}

const Title3Container = styled.h3`
    font-size: 2.1875rem;
    font-weight: 700;
    margin: 0;

    @media screen and (max-width: 1200px){
        font-size: 1.8rem;
    }
    
    @media screen and (max-width: 600px){
        font-size: 1.2rem;
    }
`;

export default connect(Title3)