import React from "react"
import { connect, styled } from "frontity"

const SectionTitle = ({ label }) => {
    return (
        <SectionTitleContainer>
            {label}
        </SectionTitleContainer>
    )
}

const SectionTitleContainer = styled.h2`
    font-family: 'Self Modern', serif;
    font-size: 6.25rem;
    font-weight: 400;
    margin: 0;

    &::before{
        background-color: currentColor;
        content: '';
        display: block;
        height: 1px;
        margin-bottom: 1.5625rem;
        width: 33%;
    }

    @media screen and (max-width: 1200px){
        font-size: 4rem;
    }
    
    @media screen and (max-width: 600px){
        font-size: 2rem;
    }
`;

export default connect(SectionTitle)