import React from "react"
import { connect, styled, css } from "frontity"

const Tags = ({ label, string }) => {
    return (
        <TagsContainer string={string} className="tags">
            {label}
        </TagsContainer>
    )
}

const TagsContainer = styled.strong`
    font-size: 2.1875rem;
    font-weight: 700;
    
    @media screen and (max-width: 1400px){
        font-size: 1.8rem;
    }
    
    @media screen and (max-width: 600px){
        font-size: 1.2rem;
    }

    ${props => props.string && css`
        align-items: center;
        column-gap: 1.875rem;
        display: flex;
            
        &::after{
            background-color: currentColor;
            content: "";
            display: block;
            height: 1px;
            flex: 1 1 8.375rem;
            margin-top: .1em;
        }
    `}
`;

export default connect(Tags)