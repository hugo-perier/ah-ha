import React from "react"
import { Global, css, connect } from "frontity"
import { AnimatePresence, motion } from "framer-motion"

import DmSans_500_eot from "../fonts/dm-sans-v11-latin-500.eot"
import DmSans_500_woff2 from "../fonts/dm-sans-v11-latin-500.woff2"
import DmSans_500_woff from "../fonts/dm-sans-v11-latin-500.woff"
import DmSans_500_ttf from "../fonts/dm-sans-v11-latin-500.ttf"
import DmSans_500_svg from "../fonts/dm-sans-v11-latin-500.svg"
import DmSans_700_eot from "../fonts/dm-sans-v11-latin-700.eot"
import DmSans_700_woff2 from "../fonts/dm-sans-v11-latin-700.woff2"
import DmSans_700_woff from "../fonts/dm-sans-v11-latin-700.woff"
import DmSans_700_ttf from "../fonts/dm-sans-v11-latin-700.ttf"
import DmSans_700_svg from "../fonts/dm-sans-v11-latin-700.svg"
import SelfModern_regular_eot from "../fonts/SelfModern-Regular.eot"
import SelfModern_regular_svg from "../fonts/SelfModern-Regular.svg"
import SelfModern_regular_ttf from "../fonts/SelfModern-Regular.ttf"
import SelfModern_regular_woff from "../fonts/SelfModern-Regular.woff"
import SelfModern_regular_woff2 from "../fonts/SelfModern-Regular.woff2"
import SelfModern_italic_eot from "../fonts/SelfModern-Italic.eot"
import SelfModern_italic_svg from "../fonts/SelfModern-Italic.svg"
import SelfModern_italic_ttf from "../fonts/SelfModern-Italic.ttf"
import SelfModern_italic_woff from "../fonts/SelfModern-Italic.woff"
import SelfModern_italic_woff2 from "../fonts/SelfModern-Italic.woff2"
import Switch from "@frontity/components/switch"
import Logo from "./Molecules/Logo"
import Loading from "./Organisms/Loading"
import Menu from "./Organisms/Menu"
import Error404 from "./Templates/Error404"
import Home from "./Templates/Home"
import Page from "./Templates/Page"
import Post from "./Templates/Post"
import Archive from "./Templates/Archive"
import PageTransition from "./Utils/PageTransition"


const Root = ({ state }) => {
    const data = state.source.get(state.router.link);
    const location = state.router;

    return (
        <>
            <Global styles={globalStyles} />
            <Logo />
            {/* <Menu /> */}
            <AnimatePresence exitBeforeEnter>
                <Switch location={location} key={location.link}>
                    <Loading when={data.isFetching} />
                    <PageTransition when={data.isHome} component={<Home url={location.link} />} />
                    <PageTransition when={data.isPage} component={<Page url={location.link} />} />
                    <PageTransition when={data.isProjetArchive} component={<Archive url={location.link} />} />
                    <PageTransition when={data.isArchive} component={<Archive url={location.link} />} />
                    <PageTransition when={data.isProjet} component={<Post url={location.link} />} />
                    <PageTransition when={data.isError} component={<Error404 url={location.link} />} />
                </Switch>
            </AnimatePresence>
        </>
    )
}

const globalStyles = css`
    :root {
        --dark: #3E3E3E;
        --light: #FFF9ED;

        --body-margin: 5%;
    }

    /* dm-sans-500 - latin */
    @font-face {
    font-family: 'DM Sans';
    font-style: normal;
    font-weight: 500;
    src: url("${DmSans_500_eot}"); /* IE9 Compat Modes */
    src: local(''),
        url("${DmSans_500_woff2}") format('woff2'), /* Super Modern Browsers */
        url("${DmSans_500_woff}") format('woff'), /* Modern Browsers */
        url("${DmSans_500_ttf}") format('truetype'), /* Safari, Android, iOS */
        url("${DmSans_500_svg}") format('svg'); /* Legacy iOS */
    }

    /* dm-sans-700 - latin */
    @font-face {
    font-family: 'DM Sans';
    font-style: normal;
    font-weight: 700;
    src: url("${DmSans_700_eot}");
    src: local(''),
        url("${DmSans_700_woff2}") format('woff2'),
        url("${DmSans_700_woff}") format('woff'),
        url("${DmSans_700_ttf}") format('truetype'),
        url("${DmSans_700_svg}") format('svg');
    }

    /* Self Modern - regular */
    @font-face {
        font-family: 'Self Modern';
        src: url('${SelfModern_regular_eot}');
        src: url('${SelfModern_regular_woff2}') format('woff2'),
            url('${SelfModern_regular_woff}') format('woff'),
            url('${SelfModern_regular_ttf}') format('truetype'),
            url('${SelfModern_regular_svg}') format('svg');
        font-weight: normal;
        font-style: normal;
        font-display: swap;
    }

    /* Self Modern - italic */
    @font-face {
        font-family: 'Self Modern';
        src: url('${SelfModern_italic_eot}');
        src: url('${SelfModern_italic_woff2}') format('woff2'),
            url('${SelfModern_italic_woff}') format('woff'),
            url('${SelfModern_italic_ttf}') format('truetype'),
            url('${SelfModern_italic_svg}') format('svg');
        font-weight: normal;
        font-style: italic;
        font-display: swap;
    }

    html {
        scroll-behavior: smooth;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    body {
        background-color: var(--light);
        color: var(--dark);
        font-family: 'DM Sans', sans-serif;
        font-weight: 500;
        margin: 3.75rem 0 0;
    }

    main{
        margin: 6.25rem 0;
    }

    img{
        max-width: 100%;
    }
`;

export default connect(Root)