import React from "react"
import { connect, styled } from "frontity"

const Post = ({ state, libraries, url }) => {
    const data = state.source.get(url);
    const page = state.source[data.type][data.id];
    const Html2React = libraries.html2react.Component;

    return (
        <PostContainer>
            <Html2React html={page.content.rendered} />
        </PostContainer>
    )
}

const PostContainer = styled.main`
    margin-bottom: 0;
`;

export default connect(Post)