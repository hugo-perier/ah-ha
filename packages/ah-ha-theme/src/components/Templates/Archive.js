import React from "react"
import { connect, styled } from "frontity"

import Article from "../Organisms/Article"

const Archive = ({ state, url }) => {
    const data = state.source.get(url);

    return (
        <ListContainer>
            <h1>salut</h1>
            {data.items.map(({ type, id }) => {
                const item = state.source[type][id];
                return <Article key={item.id} item={item} />;
            })}
        </ListContainer>
    )
}

const ListContainer = styled.main`
    display: flex;
    flex-direction: column;
    row-gap: 6.25rem;

    article:nth-of-type(even){
        flex-direction: row-reverse;
    }
`;


export default connect(Archive)