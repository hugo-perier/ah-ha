import React from "react"
import { connect, styled } from "frontity"
import ImageText from "../Organisms/ImageText"
import TwoColumn from "../Organisms/TwoColumn"
import Contact from "../Organisms/Contact"

const Page = () => {

    return (
        <>
            <ImageText />
            <TwoColumn />
            <Contact />
        </>

    )
}

export default connect(Page)