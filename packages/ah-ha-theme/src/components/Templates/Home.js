import React from "react"
import { connect, styled } from "frontity"

import Cta from "../Molecules/Cta"

const Home = ({ state, libraries, url }) => {
    const data = state.source.get(url);
    const page = state.source[data.type][data.id];
    const Html2React = libraries.html2react.Component;

    return (
        <HomeContainer>
            <Html2React html={page.content.rendered} />
            <Cta anchor="/projets" label="Voir mes projets" />
        </HomeContainer>

    )
}

const HomeContainer = styled.main`
    align-items: center;
    display: flex;
    flex-direction: column;
    row-gap: 6.25rem;
`;

export default connect(Home)