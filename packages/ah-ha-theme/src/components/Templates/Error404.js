import React from "react"
import { connect, styled } from "frontity"

const Error404 = ({ state }) => {
    return (
        <>
            <ErrorHeader className="container">
                <h1>Inspirez, expirez.</h1>
                <p>Erreur 404, Le lien <em>{state.router.link}</em> n'existe pas…</p>
            </ErrorHeader>
        </>
    );
};

const ErrorHeader = styled.header`
    h1{
        font-size: 3.125rem;
        margin: 0;
    }
`;

export default connect(Error404)