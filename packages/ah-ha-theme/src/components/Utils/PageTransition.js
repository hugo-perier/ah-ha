import React from "react"
import { connect } from "frontity"
import { motion } from "framer-motion"

import Overlay from "../Organisms/Overlay"

const TemplateContainer = ({ component }) => {
    return (
        <motion.div
            initial="initial"
            animate="in"
            exit="out"
            variants={pageVariants}
            transition={{ duration: .5 }}
        >
            {component}
            <Overlay />
        </motion.div>
    )
}

const pageVariants = {
    initial: {
        opacity: 1
    },
    in: {
        opacity: 1
    },
    out: {
        opacity: 1
    }
};

export default connect(TemplateContainer)